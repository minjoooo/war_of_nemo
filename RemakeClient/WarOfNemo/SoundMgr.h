#pragma once
#include <io.h>

class SoundMgr
{
private:
	explicit SoundMgr();
	virtual ~SoundMgr();

public:
	enum CHANNEL_ID {BGM, FIRE, UI_BUTTON, MAX_CHANNEL /*텍스트 입력할때, 플레이어 워킹, 플레이어 어택 등등,, 따로구분해놓는게좋다.*/ };

public:
	void ReadySoundManager(const char* pParentFolderPath);
	void UpdateSoundManager();

public:
	void PlaySound(const TCHAR* pSoundKey, CHANNEL_ID eID);
	void PlaySoundLoop(const TCHAR* pSoundKey, CHANNEL_ID eID);
	void PlayBGM(const TCHAR* pSoundKey); // PlayBGM 노래 무한반복
	void StopSound(CHANNEL_ID eID);
	void StopAll();

private:
	void LoadSoundFile(const char* pParentFolderPath);

private:
	std::map<const TCHAR*, FMOD_SOUND*>		m_MapSound;
	FMOD_CHANNEL*							m_pChannelArr[MAX_CHANNEL];
	FMOD_SYSTEM*							m_pSystem;

public:
	static SoundMgr*	GetInstance();
	static void			DestroyInstance();

private:
	static SoundMgr* m_pInstance;
};

