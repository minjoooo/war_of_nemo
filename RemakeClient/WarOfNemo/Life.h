#pragma once
#include "GameObject.h"

class Texture;
class TransformComponent;
class Life final : public GameObject
{
public:
	explicit Life();
	virtual ~Life();

public:
	virtual HRESULT			Initialize() override;
	virtual int				Update(const float& DeltaTime) override;
	virtual void			Render() override;

private:
	virtual void			Release() override;

private:
	Texture*				m_pTexture;
	D2D1_RECT_F				m_Rect[TOTAL_LIFE_NUM];
	D2D1::Matrix3x2F		m_Matrix;
};

