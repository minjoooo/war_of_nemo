#pragma once
#include "GameObject.h"

class Texture;
class GameoverUI final : public GameObject
{
public:
	explicit GameoverUI();
	virtual ~GameoverUI();

public:
	virtual HRESULT Initialize() override;
	virtual int Update(const float& DeltaTime) override;
	virtual void Render() override;

private:
	virtual void Release() override;

private:
	Texture* m_pBackground;
	Texture* m_pWinner;

	D2D1_RECT_F	m_Rect;
};

