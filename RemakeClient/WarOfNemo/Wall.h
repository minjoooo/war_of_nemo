#pragma once
#include "GameObject.h"

class Texture;
class TransformComponent;
class Wall final : public GameObject
{
public:
	explicit Wall( );
	virtual ~Wall();

public:
	virtual HRESULT			Initialize() override;
	virtual int				Update( const float& DeltaTime ) override;
	virtual void			Render() override;

private:
	virtual void			Release() override;

public:
	// SetMesh(Static / Dynamic)
	// SetMaterial(have and set Texture)
	// SetShader(for Build PSO)

private:
	int						m_ID;
	D2D1::Matrix3x2F		m_Matrix;
	int						iskey = false;

	// Component
	TransformComponent* m_TransformComp;

	// Mesh / Material(Texture) / Shader
	Texture* m_pTexture;
	D2D1_RECT_F				m_Rect;


};

