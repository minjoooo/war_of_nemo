#include "framework.h"
#include "Info.h"
#include "PlayerInfo.h"

void Info::Initialize()
{
	for (int i = 0; i < TOTAL_PLAYER_NUM; ++i)
	{
		m_PlayersInfo[i] = nullptr;
	}
	m_ClientsNum = 0;
	m_IsConnect = false;
	m_GameStart = false;

	m_BulletsInfo = new Pro_Bullet[TOTAL_BULLET_NUM];
}

void Info::CreatePlayerInfo(char id, bool isMe)
{
	m_ClientsNum++;
	PlayerInfo* pPlayerInfo = new PlayerInfo;
	pPlayerInfo->SetID(id, isMe);
	m_PlayersInfo[static_cast<int>(id)] = pPlayerInfo;
}

void Info::DeletePlayerInnfo(char id)
{
	m_ClientsNum--;
	delete m_PlayersInfo[static_cast<int>(id)];
	m_PlayersInfo[static_cast<int>(id)] = nullptr;
}

