#include "framework.h"
#include "Bullet.h"
#include "Texture.h"

#include "TransformComponent.h"
#include "RendererComponent.h"

#include "Info.h"


Bullet::Bullet()
{
	ZeroMemory(&m_Rect, sizeof(m_Rect));

	// Texture
	m_pTexture = new Texture(L"./Textures/bullet.bmp");
	m_Size = 3;
}

Bullet::~Bullet()
{
	this->Release();
}

HRESULT Bullet::Initialize()
{
	return S_OK;
}

int Bullet::Update(const float& DeltaTime)
{
	GameObject::UpdateComponents(DeltaTime);
	RendererComponent::GetInstance()->AddRenderLayer(RendererComponent::OBJLAYER::OBJ_PRIORITY, this);

	return 0;
}

void Bullet::Render()
{
	for (int i = 0; i < 100; ++i) {
		m_Rect.top = Info::GetInstance()->m_BulletsInfo[i].posY - m_Size;
		m_Rect.left = Info::GetInstance()->m_BulletsInfo[i].posX - m_Size;
		m_Rect.bottom = Info::GetInstance()->m_BulletsInfo[i].posY + m_Size;
		m_Rect.right = Info::GetInstance()->m_BulletsInfo[i].posX + m_Size;

		if(Info::GetInstance()->m_BulletsInfo[i].posX != -1 && Info::GetInstance()->m_BulletsInfo[i].posX != -1){
			if (m_pTexture->GetBitmap() != NULL) {
				g_pRendertarget->DrawBitmap(m_pTexture->GetBitmap(), &m_Rect);
			}
		}
	}
}

void Bullet::Release()
{
	RendererComponent::GetInstance()->ClearRenderLayer();
	GameObject::ClearComponents();
}
