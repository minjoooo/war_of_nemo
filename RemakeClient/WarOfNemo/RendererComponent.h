#pragma once
#include "Component.h"

class GameObject;
class RendererComponent final : public Component
{
public:
	enum class OBJLAYER { OBJ_PRIORITY, OBJ_UI, OBJ_BACKGROUND, OBJ_END };

private:
	explicit RendererComponent();
	virtual ~RendererComponent();

public:
	virtual HRESULT InitializeComponent() override;
	virtual void	UpdateComponent(const float& fTimeDelta) override;
	virtual void	ReleaseComponent() override;

public:
	void AddRenderLayer(const OBJLAYER& eLayer, GameObject* pObj);
	void ClearRenderLayer();

public:
	void RenderLayer();

public:
	static RendererComponent* GetInstance();
	static void DestroyInstance();

private:
	static RendererComponent* m_pInstance;
	std::list<GameObject*>	m_ObjectList[static_cast<int>(OBJLAYER::OBJ_END)];
	
	// rootsiganture
};

