#pragma once
#include "Mesh.h"

class StaticMesh final : public Mesh
{
public:
	explicit StaticMesh();
	virtual ~StaticMesh();

public:
	void DrawRectangle(D2D1_RECT_F rect, D2D1::ColorF color);
	void DrawCircle(D2D1_RECT_F rect, FLOAT size, D2D1::ColorF color);
};

