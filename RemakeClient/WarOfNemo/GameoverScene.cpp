#include "framework.h"
#include "GameoverScene.h"
#include "Network.h"
#include "SoundMgr.h"
#include "SceneMgr.h"
#include "LobbyScene.h"
#include "Info.h" 
#include "Texture.h"
#include "GameoverUI.h"
#include "Info.h"
#include "PlayerInfo.h"

GameoverScene::GameoverScene()
{
}

GameoverScene::~GameoverScene()
{
	this->ReleaseScene();
}

HRESULT GameoverScene::InitializeScene()
{
	std::cout << "Init GameoverScene" << std::endl;

	// GameoverUI
	GameObject* pGameoverUI = new GameoverUI;
	pGameoverUI->Initialize();
	m_pObjectMgr->Add_Object(ObjectMgr::OBJID::ID_UI, pGameoverUI);

	SoundMgr::GetInstance()->PlayBGM(L"end.wav");

	return S_OK;
}

void GameoverScene::UpdateScene(const float& fTimeDelta)
{
	if (m_pObjectMgr != nullptr)
		m_pObjectMgr->Update_ObjectMgr(fTimeDelta);

	Network::GetInstance()->Recv();
}

void GameoverScene::ReleaseScene()
{
	STD cout << "게임오버씬 릴리즈 중,,," << STD endl;
}

LRESULT GameoverScene::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_KEYUP:
		if (wParam == VK_SPACE)
		{
			Info::GetInstance()->m_GameStart = false;
			for (int i = 0; i < 3; ++i)
			{
				if(Info::GetInstance()->m_PlayersInfo[i] != nullptr)
					Info::GetInstance()->m_PlayersInfo[i]->Initialize();
			}

			SoundMgr::GetInstance()->PlaySound(L"shoot2.wav", SoundMgr::CHANNEL_ID::UI_BUTTON);
			SoundMgr::GetInstance()->StopAll();
			ObjectMgr::GetInstance()->Clear_All();
			Scene* lobbyScene = new LobbyScene();
			lobbyScene->InitializeScene();
			SceneMgr::GetInstance()->SceneChange(lobbyScene);
		}
		break;
	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE)
			PostQuitMessage(0);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}
