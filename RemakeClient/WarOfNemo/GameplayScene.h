#pragma once
#include "Scene.h"

class GameplayScene final : public Scene
{
public:
	explicit GameplayScene();
	virtual ~GameplayScene();

public:
	virtual HRESULT InitializeScene() override;
	virtual void UpdateScene( const float& fTimeDelta ) override;
	virtual void ReleaseScene() override;

	virtual LRESULT CALLBACK WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam );

private:
	bool CanShootBullet()
	{
		if (m_RemainingBulletCoolTime < 0.00001f)
		{
			m_RemainingBulletCoolTime = m_DefaultBulletCoolTiem;
			return true;
		}
		return false;
	}

	void Shoot( int type );

private:
	bool isWTrue = false;
	bool isSTrue = false;
	bool isATrue = false;
	bool isDTrue = false;

	bool isUpTrue = false;
	bool isDownTrue = false;
	bool isLeftTrue = false;
	bool isRightTrue = false;

	float m_RemainingBulletCoolTime = 0.f;
	float m_DefaultBulletCoolTiem = 0.3f;
};

