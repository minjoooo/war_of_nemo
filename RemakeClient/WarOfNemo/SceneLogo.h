#pragma once
#include "Scene.h"


//final 더이상 상속받을 애가 없다.
class SceneLogo final : public Scene
{
public:
	explicit SceneLogo();
	virtual ~SceneLogo();

public:
	virtual HRESULT InitializeScene() override;
	virtual void UpdateScene(const float& fTimeDelta) override;
	virtual void ReleaseScene() override;

	virtual LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	bool m_IsConnect;
	bool m_IsKey;

};

