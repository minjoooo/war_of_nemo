#include "framework.h"
#include "Life.h"
#include "Texture.h"
#include "Info.h"
#include "PlayerInfo.h"
#include "RendererComponent.h"
#include "TransformComponent.h"

Life::Life() :
	m_pTexture(nullptr)
{
	ZeroMemory(&m_Rect, sizeof(m_Rect));
}

Life::~Life()
{
	this->Release();
}

HRESULT Life::Initialize()
{
	// Transform Component
	m_Matrix._31 = 20;
	m_Matrix._32 = 20;

	m_pTexture = new Texture(L"./Textures/life.png");

	return S_OK;
}

int Life::Update(const float& DeltaTime)
{
	GameObject::UpdateComponents(DeltaTime);
	RendererComponent::GetInstance()->AddRenderLayer(RendererComponent::OBJLAYER::OBJ_PRIORITY, this);

	return 0;
}

void Life::Render()
{
	FLOAT size = 5.f;

	for (int i = 0; i < Info::GetInstance()->m_PlayersInfo[Info::GetInstance()->m_ClientID]->GetLife() + 1; ++i) {
		m_Rect[i].top = m_Matrix._32 - size;
		m_Rect[i].left = m_Matrix._31 + i * 20 - size;
		m_Rect[i].bottom = m_Matrix._32 + size;
		m_Rect[i].right = m_Matrix._31 + i * 20 + size;

		if (m_pTexture->GetBitmap() != NULL)
			g_pRendertarget->DrawBitmap(m_pTexture->GetBitmap(), &m_Rect[i]);
	}
}

void Life::Release()
{
	RendererComponent::GetInstance()->ClearRenderLayer();
	GameObject::ClearComponents();
}
