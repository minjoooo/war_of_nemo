#pragma once
#include "Component.h"

class TransformComponent final : public Component
{
private:
	explicit TransformComponent();
	virtual ~TransformComponent();

public:
	void				SetPosition(const D2D1::Matrix3x2F& matPos);
	void				SetSize(const float& fSize);
	void				SetVelocity(const float& fVelocity);

	D2D1::Matrix3x2F	GetPosition() const;
	float				GetSize() const;
	float				GetVelocity() const;

	void				MoveUp();
	void				MoveDown();
	void				MoveLeft();
	void				MoveRight();

	void				Rotate();


public:
	virtual HRESULT		InitializeComponent() override;
	virtual void		UpdateComponent(const float& fTimeDelta) override;
	virtual void		ReleaseComponent() override;

public:
	static TransformComponent* CreateComponent();
	DIRECTION	m_tDir;

private:
	D2D1::Matrix3x2F m_WorldMat;

	float		m_Size;
	float		m_Velocity;
	float		m_Acceleration;
};