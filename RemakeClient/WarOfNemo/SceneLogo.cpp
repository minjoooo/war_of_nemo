#include "framework.h"
#include "SceneLogo.h"

#include "SoundMgr.h"
#include "SceneMgr.h"
#include "LobbyScene.h"

#include "Network.h"
#include "Info.h"

#include "RendererComponent.h"
#include "LogoUI.h"

SceneLogo::SceneLogo() : m_IsConnect( false ), m_IsKey( false )
{
}

SceneLogo::~SceneLogo()
{
	ReleaseScene();
}

HRESULT SceneLogo::InitializeScene()
{
	// 家南积己
	Network::GetInstance()->InitSocket();

	// LogoUI
	GameObject* pLogoUI = new LogoUI;
	pLogoUI->Initialize();
	m_pObjectMgr->Add_Object( ObjectMgr::OBJID::ID_UI, pLogoUI );

	// BGM 积己
	SoundMgr::GetInstance()->PlayBGM( L"logo.wav" );

	return S_OK;
}

void SceneLogo::UpdateScene( const float& fTimeDelta )
{
	if ( m_pObjectMgr != nullptr )
		m_pObjectMgr->Update_ObjectMgr( fTimeDelta );

	Network::GetInstance()->Recv();
	if ( Info::GetInstance()->m_IsConnect )
	{
		SoundMgr::GetInstance()->StopAll();
		ObjectMgr::GetInstance()->Clear_All();
		Scene* lobbyScene = new LobbyScene();
		lobbyScene->InitializeScene();
		SceneMgr::GetInstance()->SceneChange( lobbyScene );
	}
}

void SceneLogo::ReleaseScene()
{
	STD cout << "肺绊纠 副府令 吝,,," << STD endl;
}

LRESULT SceneLogo::WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch ( message )
	{
	case WM_KEYUP:
		if ( wParam == VK_RETURN ) {
			SoundMgr::GetInstance()->PlaySound( L"ready.wav", SoundMgr::CHANNEL_ID::UI_BUTTON );
			Network::GetInstance()->Connect( &inputIPAddr );
		}
		if ( wParam == 8 )//backspace
			inputIPAddr = inputIPAddr.substr( 0, inputIPAddr.length() - 1 );
		if (inputIPAddr.size() < 15) {
			if (wParam == '0' || wParam == 96)
				inputIPAddr += '0';
			if (wParam == '1' || wParam == 97)
				inputIPAddr += '1';
			if (wParam == '2' || wParam == 98)
				inputIPAddr += '2';
			if (wParam == '3' || wParam == 99)
				inputIPAddr += '3';
			if (wParam == '4' || wParam == 100)
				inputIPAddr += '4';
			if (wParam == '5' || wParam == 101)
				inputIPAddr += '5';
			if (wParam == '6' || wParam == 102)
				inputIPAddr += '6';
			if (wParam == '7' || wParam == 103)
				inputIPAddr += '7';
			if (wParam == '8' || wParam == 104)
				inputIPAddr += '8';
			if (wParam == '9' || wParam == 105)
				inputIPAddr += '9';
			if (wParam == 190 || wParam == 110)	//柯痢
				inputIPAddr += '.';
		}
		break;
	case WM_KEYDOWN:
		if ( wParam == VK_ESCAPE )
			PostQuitMessage( 0 );
		break;
	case WM_DESTROY:
		PostQuitMessage( 0 );
		break;
	default:
		return DefWindowProc( hWnd, message, wParam, lParam );
	}
	return 0;
}
