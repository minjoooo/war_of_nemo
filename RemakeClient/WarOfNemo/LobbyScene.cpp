#include "framework.h"
#include "LobbyScene.h"

#include "SoundMgr.h"
#include "SceneMgr.h"
#include "GameplayScene.h"

#include "Network.h"
#include "Info.h"
#include "PlayerInfo.h"

#include "Player.h"
#include "LobbyUI.h"

LobbyScene::LobbyScene()
{
}

LobbyScene::~LobbyScene()
{
	ReleaseScene();
}

HRESULT LobbyScene::InitializeScene()
{
	std::cout << "Init LobbyScene" << std::endl;

	// LobbyUI
	GameObject* pLogoUI = new LobbyUI;
	pLogoUI->Initialize();
	m_pObjectMgr->Add_Object(ObjectMgr::OBJID::ID_UI, pLogoUI);

	SoundMgr::GetInstance()->PlayBGM(L"lobby.wav");

	return S_OK;
}

void LobbyScene::UpdateScene( const float& fTimeDelta )
{
	if ( m_pObjectMgr != nullptr )
		m_pObjectMgr->Update_ObjectMgr( fTimeDelta );

	Network::GetInstance()->Recv();
	if ( Info::GetInstance()->m_GameStart )
	{
		SoundMgr::GetInstance()->StopAll();
		ObjectMgr::GetInstance()->Clear_All();
		Scene* gameplayScene = new GameplayScene();
		gameplayScene->InitializeScene();
		SceneMgr::GetInstance()->SceneChange( gameplayScene );
	}
}

void LobbyScene::ReleaseScene()
{
	STD cout << "로비씬 릴리즈 중,,," << STD endl;
}

LRESULT LobbyScene::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{

	switch (message)
	{
	case WM_KEYUP:
		if (wParam == VK_SPACE)
		{
			SoundMgr::GetInstance()->PlaySound(L"ready.wav", SoundMgr::CHANNEL_ID::UI_BUTTON);
			Network::GetInstance()->Send(CS_READY);
		}
		break;
	case WM_KEYDOWN:
		if (wParam == VK_ESCAPE)
			PostQuitMessage(0);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;

}
