#pragma once

class Component;
class GameObject abstract
{
public:
	explicit GameObject();
	virtual ~GameObject();

public:
	virtual HRESULT Initialize() = 0;
	virtual int		Update(const float& DeltaTime) = 0;
	virtual void	Render() = 0;

	void AddComponent(TCHAR* pKey, Component* pValue);
	void UpdateComponents(const float& fTimeDelta);
	void ClearComponents();

private:
	virtual void Release() = 0;

protected:
	std::unordered_map<TCHAR*, Component*> m_Components;
};

