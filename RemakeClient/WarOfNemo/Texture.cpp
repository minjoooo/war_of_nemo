#include "framework.h"
#include "Texture.h"

Texture::Texture(const wchar_t* filePath)
{
	// Texture
	LoadMyImage(filePath);
}

Texture::~Texture()
{
	this->Release();
}

ID2D1Bitmap* Texture::GetBitmap() const
{
	return m_pBitmap;
}

int Texture::LoadMyImage(const wchar_t* ap_path)
{
	if (m_pBitmap != NULL) {  // 기존에 읽은 이미지가 있으면 해당 이미지를 제거한다.
		m_pBitmap->Release();
		m_pBitmap = NULL;
	}

	// WIC(Windows Imaging Component)관련 객체를 생성하기 위한 Factory 객체 선언
	IWICImagingFactory* p_wic_factory;
	// WIC 객체를 생성하기 위한 Factory 객체를 생성한다.
	CoCreateInstance(CLSID_WICImagingFactory, NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&p_wic_factory));

	IWICBitmapDecoder* p_decoder;     // 압축된 이미지를 해제할 객체
	IWICBitmapFrameDecode* p_frame;   // 특정 그림을 선택한 객체
	IWICFormatConverter* p_converter; // 이미지 변환 객체
	int result = 0;  // 그림 파일을 읽은 결과 값 (0이면 그림 읽기 실패, 1이면 그림 읽기 성공)
	// WIC용 Factory 객체를 사용하여 이미지 압축 해제를 위한 객체를 생성
	if (S_OK == p_wic_factory->CreateDecoderFromFilename(ap_path, NULL, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &p_decoder)) {
		// 파일을 구성하는 이미지 중에서 첫번째 이미지를 선택한다.
		if (S_OK == p_decoder->GetFrame(0, &p_frame)) {
			// IWICBitmap형식의 비트맵을 ID2D1Bitmap. 형식으로 변환하기 위한 객체 생성
			if (S_OK == p_wic_factory->CreateFormatConverter(&p_converter)) {
				// 선택된 그림을 어떤 형식의 비트맵으로 변환할 것인지 설정한다.
				if (S_OK == p_converter->Initialize(p_frame, GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, NULL, 0.0f, WICBitmapPaletteTypeCustom)) {
					// IWICBitmap 형식의 비트맵으로 ID2D1Bitmap 객체를 생성한다.
					if (S_OK == g_pRendertarget->CreateBitmapFromWicBitmap(p_converter, NULL, &m_pBitmap)) result = 1;  // 성공적으로 생성한 경우
				}
				p_converter->Release();  // 이미지 변환 객체 제거
			}
			p_frame->Release();   // 그림파일에 있는 이미지를 선택하기 위해 사용한 객체 제거
		}
		p_decoder->Release();     // 압축을 해제하기 위해 생성한 객체 제거
	}
	p_wic_factory->Release();     // WIC를 사용하기 위해 만들었던 Factory 객체 제거

	return result;  // PNG 파일을 읽은 결과를 반환한다.
}

void Texture::Release()
{
	if(m_pBitmap != NULL)
		m_pBitmap->Release();
}
