#pragma once
#include "Scene.h"

class Texture;
class GameoverScene final : public Scene
{
public:
	explicit GameoverScene();
	virtual ~GameoverScene();

public:
	virtual HRESULT InitializeScene() override;
	virtual void UpdateScene(const float& fTimeDelta) override;
	virtual void ReleaseScene() override;
	virtual LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

};

