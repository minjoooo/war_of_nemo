#include "framework.h"
#include "Player.h"
#include "Texture.h"

#include "TransformComponent.h"
#include "RendererComponent.h"

#include "ObjectMgr.h"
#include "Bullet.h"

#include "SoundMgr.h"
#include "Info.h"
#include "PlayerInfo.h"

Player::Player(int id) :
	m_TransformComp(nullptr)
{
	m_ID = id;
	if(Info::GetInstance()->m_PlayersInfo[m_ID])
		m_Matrix = Info::GetInstance()->m_PlayersInfo[m_ID]->GetPosition();
	ZeroMemory(&m_Rect,sizeof(m_Rect));
}

Player::~Player()
{
	this->Release();
}

HRESULT Player::Initialize()
{
	// Transform Component
	m_TransformComp = TransformComponent::CreateComponent();
	m_TransformComp->SetPosition(m_Matrix);
	m_TransformComp->SetSize(30.f);
	m_TransformComp->SetVelocity(10.f);
	GameObject::AddComponent(L"Transform_Player",m_TransformComp);

	// Texture
	switch (m_ID)
	{
	case 0:
		m_pTexture[0] = new Texture(L"./Textures/character/c1_5.bmp");
		m_pTexture[1] = new Texture(L"./Textures/character/c1_4.bmp");
		m_pTexture[2] = new Texture(L"./Textures/character/c1_3.bmp");
		m_pTexture[3] = new Texture(L"./Textures/character/c1_2.bmp");
		m_pTexture[4] = new Texture(L"./Textures/character/c1_1.bmp");
		break;
	case 1:
		m_pTexture[0] = new Texture(L"./Textures/character/c2_5.bmp");
		m_pTexture[1] = new Texture(L"./Textures/character/c2_4.bmp");
		m_pTexture[2] = new Texture(L"./Textures/character/c2_3.bmp");
		m_pTexture[3] = new Texture(L"./Textures/character/c2_2.bmp");
		m_pTexture[4] = new Texture(L"./Textures/character/c2_1.bmp");
		break;
	case 2:
		m_pTexture[0] = new Texture(L"./Textures/character/c3_5.bmp");
		m_pTexture[1] = new Texture(L"./Textures/character/c3_4.bmp");
		m_pTexture[2] = new Texture(L"./Textures/character/c3_3.bmp");
		m_pTexture[3] = new Texture(L"./Textures/character/c3_2.bmp");
		m_pTexture[4] = new Texture(L"./Textures/character/c3_1.bmp");
		break;
	}


	return S_OK;
}

int Player::Update(const float& DeltaTime)
{
	GameObject::UpdateComponents(DeltaTime);
	RendererComponent::GetInstance()->AddRenderLayer(RendererComponent::OBJLAYER::OBJ_PRIORITY, this);
		
	if (Info::GetInstance()->m_PlayersInfo[m_ID] != nullptr)
		m_Matrix = Info::GetInstance()->m_PlayersInfo[m_ID]->GetPosition();

	m_TransformComp->SetPosition( m_Matrix );

	return 0;
}

void Player::Render()
{

	D2D1_MATRIX_3X2_F pos = m_TransformComp->GetPosition();
	FLOAT size = m_TransformComp->GetSize();

	m_Rect.top = m_Matrix._32 - size;
	m_Rect.left = m_Matrix._31 - size;
	m_Rect.bottom = m_Matrix._32 + size;
	m_Rect.right = m_Matrix._31 + size;

	if (Info::GetInstance()->m_PlayersInfo[m_ID] != nullptr)
	{
		if (Info::GetInstance()->m_PlayersInfo[m_ID]->GetLife() > -1) {
			if (m_pTexture[Info::GetInstance()->m_PlayersInfo[m_ID]->GetLife()]->GetBitmap() != NULL)
				g_pRendertarget->DrawBitmap(m_pTexture[Info::GetInstance()->m_PlayersInfo[m_ID]->GetLife()]->GetBitmap(), &m_Rect);
		}
		else 
		{
			// �׾�����
			//g_pRendertarget->DrawBitmap(m_pTexture[4]->GetBitmap(), &m_Rect);
		}
	}
}

void Player::Release()
{
	RendererComponent::GetInstance()->ClearRenderLayer();
	GameObject::ClearComponents();
}
