#pragma once
#include "TemplateSingleton.h"

class PlayerInfo;
class Info : public TemplateSingleton<Info>
{
public:
	void Initialize();
	void CreatePlayerInfo(char id, bool isMe);
	void DeletePlayerInnfo(char id);

public:
	PlayerInfo* m_PlayersInfo[TOTAL_PLAYER_NUM];
	Pro_Bullet*	m_BulletsInfo;

public:
	int	m_ClientsNum = 0;
	int m_ClientID = 0;
	int m_WinnerID = 0;

	// 게임 상태 변수
	bool	m_IsConnect = false;
	bool	m_GameStart = false;
	bool	m_GameOver = false;
};

