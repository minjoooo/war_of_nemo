#pragma once
#include "GameObject.h"

class Texture;
class LogoUI final : public GameObject
{
public:
	explicit LogoUI();
	virtual ~LogoUI();

public:
	virtual HRESULT			Initialize() override;
	virtual int				Update(const float& DeltaTime) override;
	virtual void			Render() override;

private:
	virtual void			Release() override;

private:
	Texture*								m_pTexture;
	D2D1_RECT_F								m_Rect;
	
	std::string								m_IPAdress;
	std::vector<std::pair<int,Texture*>>	m_IPContainer;
	std::vector<Texture*>					m_NumberTexture;
	std::array< D2D1_RECT_F, 16>			m_NumberTextureLocation;
};

