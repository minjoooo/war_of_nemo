#pragma once
class PlayerInfo
{
public:
	bool MYInfo;

public:
	explicit PlayerInfo();
	virtual ~PlayerInfo();

public:
	void Initialize();
	void Release();

public:
	void					SetID(char id, bool isMe);
	int						GetID()const;

	void					SetReadyState(bool readyState);
	bool					GetReadyState()const;

	void					SetPosition(float posX, float posY);
	D2D1::Matrix3x2F		GetPosition()const;

	void					SetLife(int life);
	int						GetLife() const;

private:
	int						m_ID;
	bool					m_ReadyState;
	D2D1::Matrix3x2F		m_WorldMat;
	int						m_Life;
};

