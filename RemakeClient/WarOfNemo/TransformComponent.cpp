#include "framework.h"
#include "TransformComponent.h"

TransformComponent* TransformComponent::CreateComponent()
{
	TransformComponent* pInstance = new TransformComponent;

	if(pInstance->InitializeComponent())
		return nullptr;

	return pInstance;
}

TransformComponent::TransformComponent()
{
	m_WorldMat = D2D1::Matrix3x2F::Identity();
	m_Size = 0;
	m_Velocity = 0;
}

TransformComponent::~TransformComponent()
{
	this->ReleaseComponent();
}

HRESULT TransformComponent::InitializeComponent()
{
	return S_OK;
}

void TransformComponent::UpdateComponent(const float& fTimeDelta)
{
	m_Velocity = m_Velocity *fTimeDelta;
}

void TransformComponent::ReleaseComponent()
{
}

void TransformComponent::MoveUp()
{
	m_WorldMat._32 -= m_Velocity;
}

void TransformComponent::MoveDown()
{
	m_WorldMat._32 += m_Velocity;
}

void TransformComponent::MoveLeft()
{
	m_WorldMat._31 -= m_Velocity;
}

void TransformComponent::MoveRight()
{
	m_WorldMat._31 += m_Velocity;
}

void TransformComponent::Rotate()
{
}

void TransformComponent::SetPosition(const D2D1::Matrix3x2F& matPos)
{
	m_WorldMat._31 += matPos._31;
	m_WorldMat._32 += matPos._32;
}

void TransformComponent::SetSize(const float& fSize)
{
	m_Size = fSize/2;
}

void TransformComponent::SetVelocity(const float& fVelocity)
{
	m_Velocity = fVelocity;
}

D2D1::Matrix3x2F TransformComponent::GetPosition() const
{
	return m_WorldMat;
}

float TransformComponent::GetSize() const
{
	return m_Size;
}

float TransformComponent::GetVelocity() const
{
	return m_Velocity;
}

