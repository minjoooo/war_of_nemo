#include "framework.h"
#include "Wall.h"
#include "Texture.h"

#include "TransformComponent.h"
#include "RendererComponent.h"

#include "ObjectMgr.h"
#include "Bullet.h"

#include "Info.h"

Wall::Wall() : m_TransformComp( nullptr ), m_pTexture( nullptr )
{
	
}

Wall::~Wall()
{
	this->Release();
}

HRESULT Wall::Initialize()
{
	// Transform Component
	m_Matrix._31 = 100;
	m_Matrix._32 = 100;
	m_TransformComp = TransformComponent::CreateComponent();
	m_TransformComp->SetPosition( m_Matrix );
	m_TransformComp->SetSize( 60.f );
	GameObject::AddComponent( L"Transform_Player", m_TransformComp );

	// Texture
	m_pTexture = new Texture( L"./Textures/wall1.bmp" );

	return S_OK;
}

int Wall::Update( const float& DeltaTime )
{
	GameObject::UpdateComponents( DeltaTime );
	RendererComponent::GetInstance()->AddRenderLayer( RendererComponent::OBJLAYER::OBJ_PRIORITY, this );

	return 0;
}

void Wall::Render()
{
	D2D1_MATRIX_3X2_F pos = m_TransformComp->GetPosition();
	FLOAT size = m_TransformComp->GetSize();

	for(int i=0;i<3;++i )
		for ( int j = 0; j < 3; ++j )
		{
			m_Rect.top = pos._32 + i * 150 - size;
			m_Rect.left = pos._31 + j * 150 - size;
			m_Rect.bottom = pos._32 + i * 150 + size;
			m_Rect.right = pos._31 + j * 150 + size;
			if ( m_pTexture->GetBitmap() != NULL )
				g_pRendertarget->DrawBitmap( m_pTexture->GetBitmap(), &m_Rect );
		}
}

void Wall::Release()
{
	RendererComponent::GetInstance()->ClearRenderLayer();
	GameObject::ClearComponents();
}
