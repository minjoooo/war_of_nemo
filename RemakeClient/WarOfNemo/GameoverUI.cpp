#include "framework.h"
#include "GameoverUI.h"
#include "RendererComponent.h"
#include "Texture.h"
#include "Info.h"
#include "PlayerInfo.h"

GameoverUI::GameoverUI()
{
	ZeroMemory(&m_Rect, sizeof(m_Rect));
}

GameoverUI::~GameoverUI()
{
	this->Release();
}

HRESULT GameoverUI::Initialize()
{
	m_pBackground = new Texture(L"./Textures/gameover.bmp");

	switch (Info::GetInstance()->m_WinnerID)
	{
	case 0:
		m_pWinner = new Texture(L"./Textures/character/c1_1.bmp");
		break;
	case 1:
		m_pWinner = new Texture(L"./Textures/character/c2_1.bmp");
		break;
	case 2:
		m_pWinner = new Texture(L"./Textures/character/c3_1.bmp");
		break;
	}

	return S_OK;
}

int GameoverUI::Update(const float& DeltaTime)
{
	GameObject::UpdateComponents(DeltaTime);
	RendererComponent::GetInstance()->AddRenderLayer(RendererComponent::OBJLAYER::OBJ_UI, this);

	return 0;
}

void GameoverUI::Render()
{
	// Background
	m_Rect.top = 0;
	m_Rect.left = 0;
	m_Rect.bottom = WINDOW_SIZE_Y;
	m_Rect.right = WINDOW_SIZE_X;

	if (m_pBackground->GetBitmap() != NULL)
		g_pRendertarget->DrawBitmap(m_pBackground->GetBitmap(), &m_Rect);

	m_Rect.top = 230;
	m_Rect.left = 170;
	m_Rect.bottom = 370;
	m_Rect.right = 335;
	if (m_pWinner->GetBitmap() != NULL)
		g_pRendertarget->DrawBitmap(m_pWinner->GetBitmap(), &m_Rect);
}

void GameoverUI::Release()
{
}
