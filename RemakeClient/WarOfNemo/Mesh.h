#pragma once
// Mesh Interface
class Mesh abstract
{
public:
	explicit Mesh();
	virtual ~Mesh();

protected:
	virtual void RenderMesh(D2D1_ROUNDED_RECT size);
	virtual void Release();

protected:
	ID2D1SolidColorBrush*	m_pBrush;
};

