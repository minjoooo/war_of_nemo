#include "framework.h"
#include "PlayerInfo.h"

PlayerInfo::PlayerInfo()
{
	MYInfo = false;
	m_ID = -1;
	m_ReadyState = false;
	m_Life = TOTAL_LIFE_NUM-1;
	ZeroMemory(&m_WorldMat, sizeof(m_WorldMat));
}

PlayerInfo::~PlayerInfo()
{
	this->Release();
}

void PlayerInfo::Initialize()
{
	m_ReadyState = false;
	m_Life = TOTAL_LIFE_NUM - 1;
	ZeroMemory(&m_WorldMat, sizeof(m_WorldMat));
}

void PlayerInfo::Release()
{
}

void PlayerInfo::SetID(char id, bool isMe)
{
	m_ID = static_cast<int>(id);
	MYInfo = isMe;
}

int PlayerInfo::GetID() const
{
	return m_ID;
}

void PlayerInfo::SetReadyState(bool readyState)
{
	m_ReadyState = readyState;
}

bool PlayerInfo::GetReadyState() const
{
	return m_ReadyState;
}

void PlayerInfo::SetPosition(float posX, float posY)
{
	m_WorldMat._31 = posX;
	m_WorldMat._32 = posY;
}

D2D1::Matrix3x2F PlayerInfo::GetPosition() const
{
	return m_WorldMat;
}

void PlayerInfo::SetLife(int life)
{
	m_Life = life-1;
}

int PlayerInfo::GetLife() const
{
	return m_Life;
}
