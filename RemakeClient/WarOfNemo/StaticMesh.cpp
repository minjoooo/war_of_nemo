#include "framework.h"
#include "StaticMesh.h"

StaticMesh::StaticMesh()
{
}

StaticMesh::~StaticMesh()
{
	Mesh::Release();
}

void StaticMesh::DrawRectangle(D2D1_RECT_F rect, D2D1::ColorF color)
{
	D2D1_ROUNDED_RECT geoInfo;

	geoInfo.rect = rect;
	geoInfo.radiusX = 0;
	geoInfo.radiusY = 0;

	g_pRendertarget->CreateSolidColorBrush(color,&m_pBrush);

	Mesh::RenderMesh(geoInfo);
}

void StaticMesh::DrawCircle(D2D1_RECT_F rect, FLOAT size, D2D1::ColorF color)
{
	D2D1_ROUNDED_RECT geoInfo;

	geoInfo.rect = rect;
	geoInfo.radiusX = size;
	geoInfo.radiusY = size;

	g_pRendertarget->CreateSolidColorBrush(color, &m_pBrush);

	Mesh::RenderMesh(geoInfo);
}