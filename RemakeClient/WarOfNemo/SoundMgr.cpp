#include "framework.h"
#include "SoundMgr.h"

SoundMgr* SoundMgr::m_pInstance = nullptr;
SoundMgr::SoundMgr() {}
SoundMgr::~SoundMgr() {}

void SoundMgr::ReadySoundManager(const char* pParentFolderPath)
{
	FMOD_System_Create(&m_pSystem);
	FMOD_System_Init(m_pSystem, MAX_CHANNEL, FMOD_INIT_NORMAL, nullptr);

	LoadSoundFile(pParentFolderPath);
}

void SoundMgr::UpdateSoundManager()
{
	FMOD_System_Update(m_pSystem);
}

void SoundMgr::PlaySound(const TCHAR* pSoundKey, CHANNEL_ID eID)
{
	if (m_MapSound.empty())
		return;

	auto& iter = find_if(m_MapSound.begin(), m_MapSound.end(),
		[&pSoundKey](auto& MyPair)
		{
			return !lstrcmp(pSoundKey, MyPair.first);
		});

	if (m_MapSound.end() == iter)
	{
		MessageBox(0, TEXT("Not Find SoundKey!"), TEXT("System Message"), MB_OK);
		return;
	}

	FMOD_System_PlaySound(m_pSystem, iter->second, FALSE, 0, &m_pChannelArr[eID]);
}

void SoundMgr::PlaySoundLoop(const TCHAR* pSoundKey, CHANNEL_ID eID)
{
	if (m_MapSound.empty())
		return;

	auto& iter = find_if(m_MapSound.begin(), m_MapSound.end(),
		[&pSoundKey](auto& MyPair)
		{
			return !lstrcmp(pSoundKey, MyPair.first);
		});

	if (m_MapSound.end() == iter)
	{
		_MSG_BOX("Not Find SoundKey!");
		return;
	}

	FMOD_System_PlaySound(m_pSystem, iter->second, NULL, 0, &m_pChannelArr[eID]);

	FMOD_Channel_SetMode(m_pChannelArr[eID], FMOD_LOOP_NORMAL);
}

void SoundMgr::PlayBGM(const TCHAR* pSoundKey)
{
	if (m_MapSound.empty())
		return;

	auto& iter = find_if(m_MapSound.begin(), m_MapSound.end(),
		[&pSoundKey](auto& MyPair)
		{
			return !lstrcmp(pSoundKey, MyPair.first);
		});

	if (m_MapSound.end() == iter)
	{
		MessageBox(0, TEXT("Not Find SoundKey!"), TEXT("System Message"), MB_OK);
		return;
	}

	FMOD_System_PlaySound(m_pSystem, iter->second, NULL, 0, &m_pChannelArr[BGM]);

	FMOD_Channel_SetMode(m_pChannelArr[BGM], FMOD_LOOP_NORMAL);
}

void SoundMgr::StopSound(CHANNEL_ID eID)
{
	FMOD_Channel_Stop(m_pChannelArr[eID]);
}

void SoundMgr::StopAll()
{
	for (int i = 0; i < MAX_CHANNEL; ++i)
		FMOD_Channel_Stop(m_pChannelArr[i]);
}

void SoundMgr::LoadSoundFile(const char* pParentFolderPath)
{
	// 인자로 들어온 폴더 내부의 MP3, WAV 확장자 파일 정보를 수집합니다.
	struct _finddata_t fd;

	char szFileName[128] = "";
	strcpy_s(szFileName, pParentFolderPath);
	strcat_s(szFileName, "*.mp3");

	intptr_t handle = _findfirst(szFileName, &fd);

	if (0 == handle)
	{
		_MSG_BOX("Not Found Sound Folder!");
		return;
	}

	if (handle == -1)
		goto FIND_WAVE;

	int iResult = 0;
	char szFullPath[128] = "";

	// 1) MP3 파일을 탐색합니다.
	while (-1 != iResult)
	{
		strcpy_s(szFullPath, pParentFolderPath);
		strcat_s(szFullPath, fd.name);

		FMOD_SOUND* pSound = nullptr;
		FMOD_RESULT eRes = FMOD_System_CreateSound(m_pSystem, szFullPath, FMOD_DEFAULT, NULL, &pSound);

		if (FMOD_OK == eRes)
		{
			int iLength = static_cast<int>(strlen(fd.name)) + 1;

			TCHAR* pSoundKey = new TCHAR[iLength];
			ZeroMemory(pSoundKey, sizeof(TCHAR) * iLength);

			MultiByteToWideChar(CP_ACP, 0, fd.name, iLength, pSoundKey, iLength);

			m_MapSound.insert({ pSoundKey, pSound });
		}

		iResult = _findnext(handle, &fd);
	}

FIND_WAVE:
	// 2) WAV 파일을 탐색합니다.
	strcpy_s(szFileName, pParentFolderPath);
	strcat_s(szFileName, "*.wav");

	handle = _findfirst(szFileName, &fd);

	if (0 == handle)
	{
		_MSG_BOX("Not Found Sound Folder!");
		return;
	}

	iResult = 0;
	while (-1 != iResult)
	{
		strcpy_s(szFullPath, pParentFolderPath);
		strcat_s(szFullPath, fd.name);

		FMOD_SOUND* pSound = nullptr;
		FMOD_RESULT eRes = FMOD_System_CreateSound(m_pSystem, szFullPath, FMOD_DEFAULT, NULL, &pSound);

		if (FMOD_OK == eRes)
		{
			int iLength = static_cast<int>(strlen(fd.name)) + 1;

			TCHAR* pSoundKey = new TCHAR[iLength];
			ZeroMemory(pSoundKey, sizeof(TCHAR) * iLength);

			MultiByteToWideChar(CP_ACP, 0, fd.name, iLength, pSoundKey, iLength);

			m_MapSound.insert({ pSoundKey, pSound });
		}

		iResult = _findnext(handle, &fd);
	}

	_findclose(handle);

	FMOD_System_Update(m_pSystem);
}

SoundMgr* SoundMgr::GetInstance()
{
	if (m_pInstance == nullptr)
		m_pInstance = new SoundMgr();

	return m_pInstance;
}

void SoundMgr::DestroyInstance()
{
	if (m_pInstance != nullptr)
	{
		SAFE_DELETE_PTR(m_pInstance);
	}
}
