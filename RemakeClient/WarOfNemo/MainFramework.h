#pragma once

class Network;
class RendererComponent;
class SceneMgr;
class SoundMgr;
class Info;
class MainFramework
{
public:
	MainFramework();
	virtual ~MainFramework();

public:
	HRESULT Initialize();
	void Update_Framework(const float& fTimeDelta);
	void Render_Framework();

	LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

private:
	void	Release();

private:
	Network*			m_pNetwork;
	RendererComponent*	m_pRendererComp;
	SoundMgr*			m_pSoundMgr;
	SceneMgr*			m_pSceneMgr;

private:
	Info* m_Info;
};

