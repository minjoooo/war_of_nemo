#pragma once
#include "GameObject.h"

class Texture;
class LobbyUI final : public GameObject
{
public:
	explicit LobbyUI();
	virtual ~LobbyUI();

public:
	virtual HRESULT			Initialize() override;
	virtual int				Update(const float& DeltaTime) override;
	virtual void			Render() override;

private:
	virtual void			Release() override;

private:
	Texture*	m_pTexture;
	Texture*	m_pCleintsTexture[TOTAL_PLAYER_NUM];
	Texture*	m_pReadyBox;

	D2D1_RECT_F				m_Rect;
	D2D1_RECT_F				m_CleintRect[3];
	D2D1_RECT_F				m_ReadyRect[3];

};

