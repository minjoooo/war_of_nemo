#include "framework.h"
#include "RendererComponent.h"
#include "GameObject.h"

RendererComponent* RendererComponent::m_pInstance = nullptr;
RendererComponent::RendererComponent()
{
}

RendererComponent::~RendererComponent()
{
	ReleaseComponent();
}

HRESULT RendererComponent::InitializeComponent()
{
	// Set RenderTargetComponent
	RECT r;
	GetClientRect(g_hWnd, &r);
	g_pFactory->CreateHwndRenderTarget(
		D2D1::RenderTargetProperties(),
		D2D1::HwndRenderTargetProperties(g_hWnd, D2D1::SizeU(r.right, r.bottom)),
		&g_pRendertarget); 

	return S_OK;
}

void RendererComponent::UpdateComponent(const float& fTimeDelta)
{
	//// 한번 렌더하고 다 지워주고
	//for (int i = 0; i < static_cast<int>(OBJLAYER::OBJ_END); ++i)
	//{
	//	this->RenderLayer(i);

	//	m_ObjectList[i].clear();
	//}
}

void RendererComponent::ReleaseComponent()
{
	//if (--m_nReferences <= 0)
	ClearRenderLayer();
}

void RendererComponent::AddRenderLayer(const OBJLAYER& eLayer, GameObject* pObj)
{
	if (pObj == nullptr)
		return;

	m_ObjectList[static_cast<int>(eLayer)].emplace_back(pObj);
}

void RendererComponent::ClearRenderLayer()
{
	for (int i = 0; i < static_cast<int>(OBJLAYER::OBJ_END); ++i)
	{
		m_ObjectList[i].clear();
	}
}

void RendererComponent::RenderLayer()
{
	g_pRendertarget->BeginDraw();
	g_pRendertarget->Clear(D2D1::ColorF(59/255.f, 60/255.f, 64/255.f));

	for (int i = 0; i < static_cast<int>(OBJLAYER::OBJ_END); ++i)
	{
		for (auto& pObj : m_ObjectList[i])
			if(pObj) pObj->Render();

		m_ObjectList[i].clear();
	}

	g_pRendertarget->EndDraw();
}

RendererComponent* RendererComponent::GetInstance()
{
	if (m_pInstance == nullptr)
	{
		m_pInstance = new RendererComponent();
	}

	return m_pInstance;
}

void RendererComponent::DestroyInstance()
{
	if (m_pInstance != nullptr)
	{
		SAFE_DELETE_PTR(m_pInstance);
	}
}
