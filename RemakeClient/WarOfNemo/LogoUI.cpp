#include "framework.h"
#include "LogoUI.h"
#include "Texture.h"
#include "RendererComponent.h"

LogoUI::LogoUI() : m_pTexture(nullptr)
{
	ZeroMemory(&m_Rect, sizeof(m_Rect));
}

LogoUI::~LogoUI()
{
	this->Release();
}

HRESULT LogoUI::Initialize()
{
	// Texture
	m_pTexture = new Texture(L"./Textures/title.png");

	// Test Texture
	m_NumberTexture.reserve(11);
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/0.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/1.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/2.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/3.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/4.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/5.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/6.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/7.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/8.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/9.png"));
	m_NumberTexture.emplace_back(new Texture(L"./Textures/number/jjum.png"));

	// 25 / 10
	for (int i = 0; i < 16; ++i) {
		m_NumberTextureLocation[i].top = 345;
		m_NumberTextureLocation[i].bottom = 385;
		m_NumberTextureLocation[i].left = 265 +i*15;
		m_NumberTextureLocation[i].right = 280 +i*15;
	}


	return S_OK;
}

int LogoUI::Update(const float& DeltaTime)
{
	GameObject::UpdateComponents(DeltaTime);
	RendererComponent::GetInstance()->AddRenderLayer(RendererComponent::OBJLAYER::OBJ_UI, this);
	
	return 0;
}

void LogoUI::Render()
{
	m_Rect.top = 0;
	m_Rect.left = 0;
	m_Rect.bottom = WINDOW_SIZE_Y;
	m_Rect.right = WINDOW_SIZE_X;

	if (m_pTexture->GetBitmap() != NULL)
		g_pRendertarget->DrawBitmap(m_pTexture->GetBitmap(), &m_Rect);

	for ( int i = 0; i < inputIPAddr.length(); ++i )
	{	
		int num {};
		if ( inputIPAddr[i] == '.' )
			num = 10;
		else
			num = inputIPAddr[i] - '0';
		g_pRendertarget->DrawBitmap( m_NumberTexture[num]->GetBitmap(), &m_NumberTextureLocation[i]);
	}
	
}

void LogoUI::Release()
{
	RendererComponent::GetInstance()->ClearRenderLayer();
	GameObject::ClearComponents();
}
