#pragma once
class Texture
{
public:
	explicit Texture(const wchar_t* filePath);
	virtual ~Texture();

public:
	ID2D1Bitmap* GetBitmap() const;

public:
	int LoadMyImage(const wchar_t* ap_path);

private:
	void	Release();

private:
	ID2D1Bitmap*		m_pBitmap;

	//D2D1_RECT_F			m_ImageRect;
	D2D1_POINT_2F		m_pCenterPos;

};

