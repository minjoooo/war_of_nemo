#include "framework.h"
#include "MainFramework.h"

#include "Network.h"
#include "SoundMgr.h"
#include "SceneMgr.h"
#include "SceneLogo.h"
#include "RendererComponent.h"
#include "Info.h"

MainFramework::MainFramework() : m_pSceneMgr(nullptr), m_pRendererComp(nullptr), m_pSoundMgr(nullptr), m_pNetwork(nullptr), m_Info(nullptr)
{
}

MainFramework::~MainFramework()
{
	Release();
}

HRESULT MainFramework::Initialize()
{
	// DX2D 컴포넌트 사용 프로그램 초기화
	CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
	D2D1CreateFactory(D2D1_FACTORY_TYPE_SINGLE_THREADED, &g_pFactory);

	// Network
	m_pNetwork = Network::GetInstance();

	// PlayerInfo
	m_Info = Info::GetInstance();
	m_Info->Initialize();

	// 사운드 매니저 생성
	m_pSoundMgr = SoundMgr::GetInstance();
	m_pSoundMgr->ReadySoundManager("../Sound/");

	// 렌더러 생성
	m_pRendererComp = RendererComponent::GetInstance();
	m_pRendererComp->InitializeComponent();

	// 최초의 SceneMgr의 Instance주소를 저장해놓는다.
	{
		m_pSceneMgr = SceneMgr::GetInstance();

		if (m_pSceneMgr == nullptr)
			return E_FAIL;

		Scene* pLogoScene = new SceneLogo;
		pLogoScene->InitializeScene();

		m_pSceneMgr->SceneChange(pLogoScene);
	}

	return S_OK;
}

void MainFramework::Update_Framework(const float& fTimeDelta)
{
	if (m_pSoundMgr != nullptr)
		m_pSoundMgr->UpdateSoundManager();

	if (m_pSceneMgr != nullptr)
		m_pSceneMgr->UpdateScene(fTimeDelta);
}

void MainFramework::Render_Framework()
{
	if (m_pRendererComp != nullptr)
		m_pRendererComp->RenderLayer();
}

LRESULT MainFramework::WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	RECT edit;
	edit.top = 10;
	edit.left = 10;
	edit.right = 50;
	edit.bottom = 50;

	switch (message)
	{
	case WM_KEYUP:
		if (SceneMgr::GetInstance())
			SceneMgr::GetInstance()->GetCurScene()->WndProc(hWnd,message,wParam,lParam);
		break;
	case WM_KEYDOWN:
		if ( SceneMgr::GetInstance() )
			SceneMgr::GetInstance()->GetCurScene()->WndProc( hWnd, message, wParam, lParam );

		//if (wParam == VK_ESCAPE)
		//	PostQuitMessage(0);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

void MainFramework::Release()
{
	m_pSoundMgr->DestroyInstance();
	m_pSceneMgr->DestroyInstance();
	m_pRendererComp->DestroyInstance();

	// Release DX2D
	g_pRendertarget->Release();
	g_pFactory->Release();
	CoUninitialize();
}
