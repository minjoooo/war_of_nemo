#include "framework.h"
#include "GameplayScene.h"

#include "SoundMgr.h"
#include "SceneMgr.h"
#include "GameoverScene.h"

#include "Network.h"
#include "Info.h"
#include "PlayerInfo.h"

#include "Player.h"
#include "Bullet.h"
#include "Wall.h"
#include "Life.h"

GameplayScene::GameplayScene()
{
}

GameplayScene::~GameplayScene()
{
	ReleaseScene();
}

HRESULT GameplayScene::InitializeScene()
{
	// Set CoolTime
	m_RemainingBulletCoolTime = m_DefaultBulletCoolTiem;

	// 플레이어 추가
	for ( int i = 0; i < 3; ++i )
	{
		if ( Info::GetInstance()->m_PlayersInfo[i] != nullptr )
		{
			GameObject* pPlayer = new Player( Info::GetInstance()->m_PlayersInfo[i]->GetID() );
			pPlayer->Initialize();
			m_pObjectMgr->Add_Object( static_cast< ObjectMgr::OBJID >( Info::GetInstance()->m_PlayersInfo[i]->GetID() ), pPlayer );
		}
	}

	// wall 추가
	GameObject* pWall = new Wall;
	pWall->Initialize();
	m_pObjectMgr->Add_Object( ObjectMgr::OBJID::ID_WALL, pWall );

	// Create Bullets
	GameObject* pBullet = new Bullet;
	pBullet->Initialize();
	ObjectMgr::GetInstance()->Add_Object( ObjectMgr::OBJID::ID_BULLET, pBullet );

	// Create Bullets
	GameObject* pLife = new Life;
	pLife->Initialize();
	ObjectMgr::GetInstance()->Add_Object( ObjectMgr::OBJID::ID_LIFE, pLife );


	// BGM 생성
	SoundMgr::GetInstance()->PlayBGM( L"play.wav" );

	return S_OK;
}

void GameplayScene::UpdateScene( const float& fTimeDelta )
{
	// Reduce bullet cooltime
	m_RemainingBulletCoolTime -= fTimeDelta;

	Network::GetInstance()->Recv();

	if ( m_pObjectMgr != nullptr )
		m_pObjectMgr->Update_ObjectMgr( fTimeDelta );

	if (Info::GetInstance()->m_PlayersInfo[Info::GetInstance()->m_ClientID]->GetLife() > -1) {

		if (isUpTrue || isDownTrue || isLeftTrue || isRightTrue)
		{
			if (isUpTrue && isRightTrue)
				Shoot(CS_SHOOT_RIGHT_UP);
			else if (isUpTrue && isLeftTrue)
				Shoot(CS_SHOOT_LEFT_UP);
			else if (isDownTrue && isRightTrue)
				Shoot(CS_SHOOT_RIGHT_DOWN);
			else if (isDownTrue && isLeftTrue)
				Shoot(CS_SHOOT_LEFT_DOWN);
			else if (isUpTrue)
				Shoot(CS_SHOOT_UP);
			else if (isDownTrue)
				Shoot(CS_SHOOT_DOWN);
			else if (isLeftTrue)
				Shoot(CS_SHOOT_LEFT);
			else if (isRightTrue)
				Shoot(CS_SHOOT_RIGHT);
		}
	}


	if ( false == Info::GetInstance()->m_GameStart )
	{
		SoundMgr::GetInstance()->StopAll();
		ObjectMgr::GetInstance()->Clear_All();
		Scene* gameoverScene = new GameoverScene();
		gameoverScene->InitializeScene();
		SceneMgr::GetInstance()->SceneChange( gameoverScene );
	}
}

void GameplayScene::ReleaseScene()
{
	STD cout << "게임 플레이 씬 릴리즈 중,,," << STD endl;
}

LRESULT GameplayScene::WndProc( HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch ( message )
	{
	case WM_KEYDOWN:
		if ( wParam == VK_ESCAPE )
			PostQuitMessage( 0 );
		if (Info::GetInstance()->m_PlayersInfo[Info::GetInstance()->m_ClientID]->GetLife() > -1) {
			if (wParam == 'W' || wParam == 'w')
			{
				if (!isWTrue)
				{
					Network::GetInstance()->Send(CS_PLAYER_UP_DOWN);
					isWTrue = true;
				}
			}
			if (wParam == 'S' || wParam == 's')
			{
				if (!isSTrue)
				{
					Network::GetInstance()->Send(CS_PLAYER_DOWN_DOWN);
					isSTrue = true;
				}
			}
			if (wParam == 'A' || wParam == 'a')
			{
				if (!isATrue)
				{
					Network::GetInstance()->Send(CS_PLAYER_LEFT_DOWN);
					isATrue = true;
				}
			}
			if (wParam == 'D' || wParam == 'd')
			{
				if (!isDTrue)
				{
					Network::GetInstance()->Send(CS_PLAYER_RIGHT_DOWN);
					isDTrue = true;
				}
			}
		}
		if ( wParam == VK_UP )
			isUpTrue = true;
		if ( wParam == VK_DOWN )
			isDownTrue = true;
		if ( wParam == VK_LEFT )
			isLeftTrue = true;
		if ( wParam == VK_RIGHT )
			isRightTrue = true;
		break;
	case WM_KEYUP:
		if (Info::GetInstance()->m_PlayersInfo[Info::GetInstance()->m_ClientID]->GetLife() > -1) {
			if (wParam == 'W' || wParam == 'w')
			{
				isWTrue = false;
				Network::GetInstance()->Send(CS_PLAYER_UP_UP);
			}
			if (wParam == 'S' || wParam == 's')
			{
				isSTrue = false;
				Network::GetInstance()->Send(CS_PLAYER_DOWN_UP);
			}
			if (wParam == 'A' || wParam == 'a')
			{
				isATrue = false;
				Network::GetInstance()->Send(CS_PLAYER_LEFT_UP);
			}
			if (wParam == 'D' || wParam == 'd')
			{
				isDTrue = false;
				Network::GetInstance()->Send(CS_PLAYER_RIGHT_UP);
			}
		}
		if ( wParam == VK_UP )
			isUpTrue = false;
		if ( wParam == VK_DOWN )
			isDownTrue = false;
		if ( wParam == VK_LEFT )
			isLeftTrue = false;
		if ( wParam == VK_RIGHT )
			isRightTrue = false;

		break;
	case WM_DESTROY:
		PostQuitMessage( 0 );
		break;
	default:
		return DefWindowProc( hWnd, message, wParam, lParam );
	}
	return 0;
}

void GameplayScene::Shoot( int type )
{
	if ( CanShootBullet() )
	{
		switch ( type )
		{
		case CS_SHOOT_UP:
			SoundMgr::GetInstance()->PlaySound( L"shoot.wav", SoundMgr::CHANNEL_ID::FIRE );
			Network::GetInstance()->Send( CS_SHOOT_UP );
			break;
		case CS_SHOOT_DOWN:
			SoundMgr::GetInstance()->PlaySound( L"shoot.wav", SoundMgr::CHANNEL_ID::FIRE );
			Network::GetInstance()->Send( CS_SHOOT_DOWN );
			break;
		case CS_SHOOT_LEFT:
			SoundMgr::GetInstance()->PlaySound( L"shoot.wav", SoundMgr::CHANNEL_ID::FIRE );
			Network::GetInstance()->Send( CS_SHOOT_LEFT );
			break;
		case CS_SHOOT_RIGHT:
			SoundMgr::GetInstance()->PlaySound( L"shoot.wav", SoundMgr::CHANNEL_ID::FIRE );
			Network::GetInstance()->Send( CS_SHOOT_RIGHT );
			break;
		case CS_SHOOT_RIGHT_UP:
			SoundMgr::GetInstance()->PlaySound( L"shoot.wav", SoundMgr::CHANNEL_ID::FIRE );
			Network::GetInstance()->Send( CS_SHOOT_RIGHT_UP );
			break;
		case CS_SHOOT_RIGHT_DOWN:
			SoundMgr::GetInstance()->PlaySound( L"shoot.wav", SoundMgr::CHANNEL_ID::FIRE );
			Network::GetInstance()->Send( CS_SHOOT_RIGHT_DOWN );
			break;
		case CS_SHOOT_LEFT_UP:
			SoundMgr::GetInstance()->PlaySound( L"shoot.wav", SoundMgr::CHANNEL_ID::FIRE );
			Network::GetInstance()->Send( CS_SHOOT_LEFT_UP );
			break;
		case CS_SHOOT_LEFT_DOWN:
			SoundMgr::GetInstance()->PlaySound( L"shoot.wav", SoundMgr::CHANNEL_ID::FIRE );
			Network::GetInstance()->Send( CS_SHOOT_LEFT_DOWN );
			break;
		}
	}
}
