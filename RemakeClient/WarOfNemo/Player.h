#pragma once
#include "GameObject.h"

class Texture;
class TransformComponent;
class Player final : public GameObject
{
public:
	explicit Player(int id);
	virtual ~Player();

public:
	virtual HRESULT			Initialize() override;
	virtual int				Update(const float& DeltaTime) override;
	virtual void			Render() override;

private:
	virtual void			Release() override;

private:
	int						m_ID;
	D2D1::Matrix3x2F		m_Matrix;
	int						iskey = false;

	// Component
	TransformComponent*		m_TransformComp;

	Texture*				m_pTexture[5];
	D2D1_RECT_F				m_Rect;
	
};

