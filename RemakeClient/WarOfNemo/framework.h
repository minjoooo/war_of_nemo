﻿// header.h: 표준 시스템 포함 파일
// 또는 프로젝트 특정 포함 파일이 들어 있는 포함 파일입니다.
//

#pragma once

#include "targetver.h"
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
// Windows 헤더 파일
#include <windows.h>
#include <Winsock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "ws2_32")

// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>

// C++ 런타임 헤더 파일입니다.
#include <iostream>
#include <string>
#include <array>
#include <list>
#include <vector>
#include <algorithm>
#include <unordered_map>
#include <map>
#include <assert.h>
#define USE_STD using namespace std;
#define STD std::

// Direct2D 런타임 헤더 파일입니다.
#include <d2d1.h>
#include <d2d1helper.h>
#include <dwrite.h>
#include <wincodec.h>

#pragma comment(lib, "D2D1.lib")
#pragma comment(lib, "D3D11.lib")
#pragma comment(lib, "DXGI.lib")
#pragma comment(lib, "dxguid.lib")

// 사용자 정의 헤더 파일입니다.
#include "GameTimer.h"

#include "../../protocol.h"
#include "define.h"
#include "extern.h"
// FMOD
#include "fmod.h"
#pragma comment(lib,"fmodL_vc.lib")

// 콘솔창 띄우기
#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif