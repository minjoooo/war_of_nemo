#pragma once
#include "GameObject.h"

class Texture;
class Bullet : public GameObject
{
public:
	explicit Bullet();
	virtual ~Bullet();

public:
	virtual HRESULT Initialize() override;
	virtual int Update(const float& DeltaTime) override;
	virtual void Render() override;

private:
	virtual void Release() override;

private:
	Texture*				m_pTexture;
	D2D1_RECT_F				m_Rect;
	int						m_Size;
};

