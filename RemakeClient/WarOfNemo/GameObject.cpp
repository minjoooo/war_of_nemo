#include "framework.h"
#include "GameObject.h"
#include "Component.h"

GameObject::GameObject()
{
}

GameObject::~GameObject()
{
}

void GameObject::AddComponent(TCHAR* pKey, Component* pValue)
{
	if (pValue == nullptr)
		return;

	m_Components.emplace(pKey, pValue);
}

void GameObject::UpdateComponents(const float& fTimeDelta)
{
	for (auto& comp : m_Components)
		comp.second->UpdateComponent(fTimeDelta);
}

void GameObject::ClearComponents()
{
	// map에 key값이 어디에 저장되는지 찾아보기
	for (auto& pair : m_Components)
		SAFE_DELETE_PTR(pair.second);

	m_Components.clear();
}
