#include "framework.h"
#include "Mesh.h"

Mesh::Mesh()
{
	m_pBrush = nullptr;
}

Mesh::~Mesh()
{
}

void Mesh::RenderMesh(D2D1_ROUNDED_RECT geoInfo)
{
	g_pRendertarget->FillRoundedRectangle(geoInfo,m_pBrush);
}

void Mesh::Release()
{
	// Release();
	m_pBrush->Release();
	m_pBrush = nullptr;
}
