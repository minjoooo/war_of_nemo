#include "framework.h"
#include "LobbyUI.h"
#include "Texture.h"
#include "RendererComponent.h"
#include "Info.h"
#include "PlayerInfo.h"

LobbyUI::LobbyUI() : m_pTexture(nullptr)
{
	ZeroMemory(&m_Rect, sizeof(m_Rect));
}

LobbyUI::~LobbyUI()
{
	this->Release();
}

HRESULT LobbyUI::Initialize()
{
	m_pTexture = new Texture(L"./Textures/lobby.png");
	m_pCleintsTexture[0] = new Texture(L"./Textures/character/c1_1.bmp");
	m_pCleintsTexture[1] = new Texture(L"./Textures/character/c2_1.bmp");
	m_pCleintsTexture[2] = new Texture(L"./Textures/character/c3_1.bmp");
	m_pReadyBox = new Texture(L"./Textures/readybox.png");

	for (int i = 0; i < 3; ++i)
	{
		m_CleintRect[i].top = 190 ;
		m_CleintRect[i].left = 65 + i * 150;
		m_CleintRect[i].bottom = 260;
		m_CleintRect[i].right = 135 + i * 150;
	}

	for (int i = 0; i < 3; ++i)
	{
		m_ReadyRect[i].top = 290;
		m_ReadyRect[i].left = 65 + i * 150;
		m_ReadyRect[i].bottom = 310;
		m_ReadyRect[i].right = 135 + i * 150;
	}

	return S_OK;
}

int LobbyUI::Update(const float& DeltaTime)
{
	GameObject::UpdateComponents(DeltaTime);
	RendererComponent::GetInstance()->AddRenderLayer(RendererComponent::OBJLAYER::OBJ_UI, this);

	return 0;
}

void LobbyUI::Render()
{
	// Background
	m_Rect.top = 0;
	m_Rect.left = 0;
	m_Rect.bottom = WINDOW_SIZE_Y;
	m_Rect.right = WINDOW_SIZE_X;

	if (m_pTexture->GetBitmap() != NULL)
		g_pRendertarget->DrawBitmap(m_pTexture->GetBitmap(), &m_Rect);

	// Player
	for (int i = 0; i < 3; ++i) {
		if (Info::GetInstance()->m_PlayersInfo[i] != nullptr) {
			if (m_pTexture->GetBitmap() != NULL)
				g_pRendertarget->DrawBitmap(m_pCleintsTexture[i]->GetBitmap(), &m_CleintRect[i]);
		}
	}

	// ReadyState
	for (int i = 0; i < 3; ++i) {
		if (Info::GetInstance()->m_PlayersInfo[i] != nullptr) {
			if (Info::GetInstance()->m_PlayersInfo[i]->GetReadyState())
			{
				if (m_pTexture->GetBitmap() != NULL)
					g_pRendertarget->DrawBitmap(m_pReadyBox->GetBitmap(), &m_ReadyRect[i]);
			}
		}
	}
}

void LobbyUI::Release()
{
	RendererComponent::GetInstance()->ClearRenderLayer();
	GameObject::ClearComponents();
}
