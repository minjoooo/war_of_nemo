#include "pch.h"
#include "Player.h"

Player::Player()
{
	m_keyW = false;
	m_keyS = false;
	m_keyA = false;
	m_keyD = false;

	m_life = 5;

	m_bulletCooltime = 0.f;
	m_reaminingCooltime = 0.2f;
}
Player::~Player()
{

}

void Player::SetLife( int life )
{
	m_life = life;
}
int Player::GetLife()
{
	return m_life;
}
void Player::MinusLife()
{
	m_life = m_life - 1;
}

bool Player::CanShootBullet()
{
	if ( m_reaminingCooltime < FLT_EPSILON )
	{
		return true;
	}
	return false;
}

void Player::ResetBulletCoolTime()
{
	m_reaminingCooltime = m_bulletCooltime;
}

void Player::SetKeyW( bool input )
{
	m_keyW = input;
}
void Player::SetKeyS( bool input )
{
	m_keyS = input;
}
void Player::SetKeyA( bool input )
{
	m_keyA = input;
}
void Player::SetKeyD( bool input )
{
	m_keyD = input;
}

bool Player::GetKeyW()
{
	return m_keyW;
}
bool Player::GetKeyS()
{
	return m_keyS;
}
bool Player::GetKeyA()
{
	return m_keyA;
}
bool Player::GetKeyD()
{
	return m_keyD;
}

//void Player::SetKeyUp( bool input )
//{
//	m_keyUp = input;
//}
//void Player::SetKeyDown( bool input )
//{
//	m_keyDown = input;
//}
//void Player::SetKeyLeft( bool input )
//{
//	m_keyLeft = input;
//}
//void Player::SetKeyRight( bool input )
//{
//	m_keyRight = input;
//}
//
//bool Player::GetKeyUp()
//{
//	return m_keyUp;
//}
//bool Player::GetKeyDown()
//{
//	return m_keyDown;
//}
//bool Player::GetKeyLeft()
//{
//	return m_keyLeft;
//}
//bool Player::GetKeyRight()
//{
//	return m_keyRight;
//}