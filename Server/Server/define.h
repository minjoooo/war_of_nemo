#pragma once

#define TYPE_PLAYER		0
#define TYPE_BULLET		1

#define DIR_UP			0
#define DIR_DOWN		1
#define DIR_LEFT		2
#define DIR_RIGHT		3

#define DIR_RIGHT_UP	4
#define DIR_RIGHT_DOWN	5
#define DIR_LEFT_UP		6
#define DIR_LEFT_DOWN	7

#define MAXPLAYER		3