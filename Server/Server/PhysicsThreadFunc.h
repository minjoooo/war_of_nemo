#pragma once
#include "pch.h"
#include "Global.h"
#include "Wall.h"

void CheckWallCollisionPlayer( int id, glm::vec2 pos );
void CheckWallCollisionBullets( int id, glm::vec2 pos );
void CheckBulletCollision();
int DoGarbageCollection();
void Update( float elapsedTime );

void ProcessClients()
{
	mutex player_lock;
	player_lock.lock();
	Pro_Player players[3] = { {g_initialPos[0]},{g_initialPos[1]},{g_initialPos[2]} };
	player_lock.unlock();

	Pro_Bullet bullets[100];
	for( int i = 0; i < 100; ++i ){
		bullets[i].posX = -1;
		bullets[i].posY = -1;
	}

	//Initialize walls
	
	Wall* p_wall = walls;

	for ( int i = 0; i < 9; ++i ) {
		walls[i].SetPos( glm::vec2 { wallpos[i].posX, wallpos[i].posY } );
		walls[i].SetVol( glm::vec2 { WALL_SIZE, WALL_SIZE } );
	}

	std::cout << "make Physics thread!" << std::endl;
	std::queue <Message> phyMsgQueue;
	Message phyMsg;
	glm::vec2 temp;

	for ( int i = 0; i < numOfCls; ++i )
	{
		temp = { players[i].posX, players[i].posY };
		phyPlayers.emplace_back( Player() );
		phyPlayers.emplace_back( Player() );
		phyPlayers[i].InitPlayer();
		phyPlayers[i].SetPos( temp );
	}

	// 시간측정
	using FpFloatMilliseconds = duration<float, milliseconds::period>;
	auto prev_Time = chrono::high_resolution_clock::now();
	float elapsedTime {};
	while ( true )
	{
		auto cur_Time = chrono::high_resolution_clock::now();
		elapsedTime += FpFloatMilliseconds( cur_Time - prev_Time ).count();
		prev_Time = cur_Time;

		if ( elapsedTime > 17 )
		{
			g_MsgQueueLock.lock();
			phyMsgQueue = g_MsgQueue;
			g_MsgQueueLock.unlock();

			g_MsgQueueLock.lock();
			while ( !g_MsgQueue.empty() )
				g_MsgQueue.pop();
			g_MsgQueueLock.unlock();

			while ( !phyMsgQueue.empty() )
			{
				phyMsg = phyMsgQueue.front();
				phyMsgQueue.pop();

				if ( phyMsg.type == TYPE_PLAYER )
				{
					switch ( phyMsg.dir )
					{
					case DIR_UP:
						phyPlayers[phyMsg.id].SetKeyW( phyMsg.isPushed );
						break;
					case DIR_DOWN:
						phyPlayers[phyMsg.id].SetKeyS( phyMsg.isPushed );
						break;
					case DIR_LEFT:
						phyPlayers[phyMsg.id].SetKeyA( phyMsg.isPushed );
						break;
					case DIR_RIGHT:
						phyPlayers[phyMsg.id].SetKeyD( phyMsg.isPushed );
						break;
					}
				}
				if ( phyMsg.type == TYPE_BULLET )
				{
					Bullet* addBullet = new Bullet();
					addBullet->InitBullet();
					glm::vec2 plapos = phyPlayers[phyMsg.id].GetPos();
					switch ( phyMsg.dir )
					{
					case DIR_UP:
					{
						addBullet->SetVel( glm::vec2( 0.f, -30.f ) );
						plapos.y -= 15;
						addBullet->SetPos( plapos );
						break;
					}
					case DIR_DOWN:
						addBullet->SetVel( glm::vec2( 0.f, 30.f ) );
						plapos.y += 15;
						addBullet->SetPos( plapos );
						break;
					case DIR_LEFT:
						addBullet->SetVel( glm::vec2( -30.f, 0.f ) );
						plapos.x -= 15;
						addBullet->SetPos( plapos );
						break;
					case DIR_RIGHT:
						addBullet->SetVel( glm::vec2( 30.f, 0.f ) );
						plapos.x += 15;
						addBullet->SetPos( plapos );
						break;
					case DIR_RIGHT_UP:
						addBullet->SetVel( glm::vec2( 20.f, -20.f ) );
						plapos.x += 13;
						plapos.y -= 13;
						addBullet->SetPos( plapos );
						break;
					case DIR_RIGHT_DOWN:
						addBullet->SetVel( glm::vec2( 20.f, 20.f ) );
						plapos.x += 13;
						plapos.y += 13;
						addBullet->SetPos( plapos );
						break;
					case DIR_LEFT_UP:
						addBullet->SetVel( glm::vec2( -20.f, -20.f ) );
						plapos.x -= 13;
						plapos.y -= 13;
						addBullet->SetPos( plapos );
						break;
					case DIR_LEFT_DOWN:
						addBullet->SetVel( glm::vec2( -20.f, 20.f ) );
						plapos.x -= 13;
						plapos.y += 13;
						addBullet->SetPos( plapos );
						break;
					}
					phyBullets.emplace_back( *addBullet );
				}

			}

			for ( int i = 0; i < numOfCls; ++i )
			{
				glm::vec2 force = glm::vec2( 0.f, 0.f );
				float fAmount = 100.f;

				if ( phyPlayers[i].GetKeyW() )force.y -= 1.f;
				if ( phyPlayers[i].GetKeyS() )force.y += 1.f;
				if ( phyPlayers[i].GetKeyA() )force.x -= 1.f;
				if ( phyPlayers[i].GetKeyD() )force.x += 1.f;

				float fSize = sqrtf( force.x * force.x + force.y * force.y );
				if ( fSize > 0.f )
				{
					force /= fSize;
					force *= fAmount;

					phyPlayers[i].AddForce( force, elapsedTime / 100 );
				}

			}

			Update( elapsedTime);

			int val = DoGarbageCollection();// only bullet delete
			// 게임 종료 초기화
			if ( val == 0 )	
			{
				phyPlayers.clear();
				phyBullets.clear();

				g_MsgQueueLock.lock();
				while ( !g_MsgQueue.empty() )
				{
					g_MsgQueue.pop();
				}
				g_MsgQueueLock.unlock();

				for ( int i = 0; i < 3; ++i )
				{
					g_playerReadyInfoLock.lock();
					g_playerReadyInfo[i].ready = 0;
					g_playerReadyInfoLock.unlock();
				}

				g_isPlaying = false;
				return;
			}

			for ( int i = 0; i < numOfCls; ++i ) {
				players[i].id = i;
				players[i].posX = phyPlayers[i].GetPos().x;
				players[i].posY = phyPlayers[i].GetPos().y;
			}
			for ( int i = 0; i < 100; ++i )
			{
				if ( i < phyBullets.size() )
				{
					bullets[i].posX = phyBullets[i].GetPos().x;
					bullets[i].posY = phyBullets[i].GetPos().y;
				}
				else
				{
					bullets[i].posX = -1;
					bullets[i].posY = -1;
				}
			}
			SendPos( *players );
			SendBullets( *bullets );
			elapsedTime = 0;
		}
	}
	phyPlayers.clear();
}

int DoGarbageCollection()
{
	int leftPla { 0 };
	int winner { 0 };
	for ( int i = 0; i < numOfCls; ++i )
	{
		if ( phyPlayers[i].GetLife() != 0 )
		{
			leftPla++;
			winner = i;
		}
	}
	if ( leftPla <= 1 )
	{
		SendGameOverPacket( winner );
		return 0;
	}

	std::vector<Bullet>::iterator iter;
	for ( iter = phyBullets.begin(); iter != phyBullets.end(); )
	{
		if ( iter->GetCount() <= 0 )
			iter = phyBullets.erase( iter );
		else
			iter++;
	}
	return 1; // 플레이 계속
}

void Update( float elapsedTime )
{
	for( int i = 0; i < numOfCls; ++i )
	{
		phyPlayers[i].Update( elapsedTime / 100 );
		CheckWallCollisionPlayer( i, phyPlayers[i].GetPos() );
	}

	for( int i = 0; i < phyBullets.size(); ++i )
	{
		phyBullets[i].Update( elapsedTime / 100 );
		CheckWallCollisionBullets( i, phyBullets[i].GetPos() );
	}
	CheckBulletCollision();
}

void CheckBulletCollision()
{
	std::vector<Bullet>::iterator iter;

	for ( int i = 0; i < numOfCls; ++i )
	{
		if ( phyPlayers[i].GetLife() != 0 )
		{
			glm::vec2 Plapos = phyPlayers[i].GetPos();
			for ( iter = phyBullets.begin(); iter != phyBullets.end(); )
			{
				glm::vec2 Bulpos = iter->GetPos();
				float d = sqrtf( ( Plapos.x - Bulpos.x ) * ( Plapos.x - Bulpos.x ) + ( Plapos.y - Bulpos.y ) * ( Plapos.y - Bulpos.y ) );
				if ( d < 14.f )
				{
					iter = phyBullets.erase( iter );
					phyPlayers[i].MinusLife();
					SendHitPacket( i, phyPlayers[i].GetLife() );
				}
				else
					iter++;
			}
		}
	}
}

void CheckWallCollisionBullets( int id, glm::vec2 pos)
{
	glm::vec2 vol = phyBullets[id].GetVol();
	//with world
	if ( pos.y + vol.y / 2 > WORLD_UP )
	{
		phyBullets[id].SetPosY( WORLD_UP - vol.y / 2 );
		phyBullets[id].InverseVY();
		phyBullets[id].MinusCount();
	}
	if ( pos.y - vol.y / 2 < WORLD_DOWN )
	{
		phyBullets[id].SetPosY( WORLD_DOWN + vol.y / 2 );
		phyBullets[id].InverseVY();
		phyBullets[id].MinusCount();
	}
	if ( pos.x + vol.x / 2 > WORLD_RIGHT )
	{
		phyBullets[id].SetPosX( WORLD_RIGHT - vol.x / 2 );
		phyBullets[id].InverseVX();
		phyBullets[id].MinusCount();
	}
	if ( pos.x - vol.x / 2 < WORLD_LEFT )
	{
		phyBullets[id].SetPosX( WORLD_LEFT + vol.x / 2 );
		phyBullets[id].InverseVX();
		phyBullets[id].MinusCount();
	}

	// Check Collision with walls
	for ( int i = 0; i < 9; ++i )
	{
		float wallup = walls[i].GetUp();
		float walldown = walls[i].GetDown();
		float wallleft = walls[i].GetLeft();
		float wallright = walls[i].GetRight();


		if ( wallup > pos.y + vol.y / 2 );
		else if ( walldown < pos.y - vol.y / 2 );
		else if ( wallleft > pos.x + vol.x / 2 );
		else if ( wallright < pos.x - vol.x / 2 );
		else {
			glm::vec2 prePos = phyBullets[id].GetPrePos();
			if ( wallup > prePos.y )
			{
				phyBullets[id].SetPosY( wallup - vol.y / 2 );
				phyBullets[id].InverseVY();
				phyBullets[id].MinusCount();
			}
			else if ( walldown < prePos.y )
			{
				phyBullets[id].SetPosY( walldown + vol.y / 2 );
				phyBullets[id].InverseVY();
				phyBullets[id].MinusCount();
			}
			if ( wallleft > prePos.x )
			{
				phyBullets[id].SetPosX( wallleft - vol.x / 2 );
				phyBullets[id].InverseVX();
				phyBullets[id].MinusCount();
			}
			else if ( wallright < prePos.x )
			{
				phyBullets[id].SetPosX( wallright + vol.x / 2 );
				phyBullets[id].InverseVX();
				phyBullets[id].MinusCount();
			}

		}

	}
}

void CheckWallCollisionPlayer( int id, glm::vec2 pos )	// 플레이어만
{
	glm::vec2 vol = phyPlayers[id].GetVol();

	//with world
	if ( pos.y + vol.y / 2 > WORLD_UP )
	{
		phyPlayers[id].SetPosY( WORLD_UP - vol.y / 2 );
		phyPlayers[id].InverseVY();
	}
	if ( pos.y - vol.y / 2 < WORLD_DOWN )
	{
		phyPlayers[id].SetPosY( WORLD_DOWN + vol.y / 2 );
		phyPlayers[id].InverseVY();
	}
	if ( pos.x + vol.x / 2 > WORLD_RIGHT )
	{
		phyPlayers[id].SetPosX( WORLD_RIGHT - vol.x / 2 );
		phyPlayers[id].InverseVX();
	}
	if ( pos.x - vol.x / 2 < WORLD_LEFT )
	{
		phyPlayers[id].SetPosX( WORLD_LEFT + vol.x / 2 );
		phyPlayers[id].InverseVX();
	}

	// Check Collision with walls
	for ( int i = 0; i < 9; ++i )
	{
		float wallup = walls[i].GetUp() - 2;
		float walldown = walls[i].GetDown() + 2;
		float wallleft = walls[i].GetLeft() - 2;
		float wallright = walls[i].GetRight() + 2;

		if ( wallup > pos.y + vol.y / 2 );
		else if ( walldown < pos.y - vol.y / 2 );
		else if ( wallleft > pos.x + vol.x / 2 );
		else if ( wallright < pos.x - vol.x / 2 );
		else {
			glm::vec2 prePos = phyPlayers[id].GetPrePos();

			float sx = ( wallleft - prePos.x ) > ( prePos.x - wallright ) ? wallleft - prePos.x : prePos.x - wallright;
			float sy = ( wallup - prePos.y ) > ( prePos.y - walldown ) ? wallup - prePos.y : prePos.y - walldown;

			if ( sy >= sx )
			{
				if ( wallup > prePos.y )
				{
					phyPlayers[id].SetPosY( wallup - vol.y / 2 );
					phyPlayers[id].InverseVY();
				}
				else if ( walldown < prePos.y )
				{
					phyPlayers[id].SetPosY( walldown + vol.y / 2 );
					phyPlayers[id].InverseVY();
				}
			}
			else
			{
				if ( wallleft > prePos.x )
				{
					phyPlayers[id].SetPosX( wallleft - vol.x / 2 );
					phyPlayers[id].InverseVX();
				}
				else if ( wallright < prePos.x )
				{
					phyPlayers[id].SetPosX( wallright + vol.x / 2 );
					phyPlayers[id].InverseVX();
				}
			}
		}
	}
}
