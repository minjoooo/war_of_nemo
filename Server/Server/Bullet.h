#pragma once
#include "Object.h"

class Bullet :public Object
{
public:
	Bullet();
	~Bullet();

	void SetCount( int count );
	int GetCount();
	void MinusCount();

private:
	int m_count;
};

