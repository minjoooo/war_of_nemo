#include "pch.h"
#include "Bullet.h"

Bullet::Bullet()
{
	m_count = 5;
}
Bullet::~Bullet()
{

}

void Bullet::SetCount( int count )
{
	m_count = count;
}
int Bullet::GetCount()
{
	return m_count;
}
void Bullet::MinusCount()
{
	m_count = m_count - 1;
}