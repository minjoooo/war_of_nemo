#pragma once
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/euler_angles.hpp"

#define GRAVITY 9.8

class Object
{
public:
	Object();
	~Object();
	void InitPlayer();
	void InitBullet();

public:
	void SetPos( glm::vec2& pos );
	glm::vec2 GetPos();

	glm::vec2 GetPrePos();

	void SetPosX(const float& pos );
	void SetPosY(const float& pos );

	void SetVol( glm::vec2 vol );
	glm::vec2 GetVol();

	void SetVel( glm::vec2 vel );
	glm::vec2 GetVel();

	void SetAcc( glm::vec2 acc );
	glm::vec2 GetAcc();

	void SetColor( glm::vec4 color );
	glm::vec4 GetColor();

	void SetMass( float mass );
	float GetMass();

	void SetFricCoef( float fricCoef );
	float GetFricCoef();

public:
	void Update( float elapsedTime );
	void AddForce( glm::vec2 force, float elapsedTime );

	void InverseVX();
	void InverseVY();

private:
	glm::vec2 m_pos;
	glm::vec2 m_prepos;
	glm::vec2 m_vol;	//아마 필요없다 클라가 필요
	glm::vec2 m_vel;
	glm::vec2 m_acc;
	glm::vec4 m_color;	//아마 필요없다 클라가 필요
	float m_mass;
	float m_fricCoef;
};

