#pragma once
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtx/euler_angles.hpp"

class Wall
{
public:
	Wall();
	Wall(glm::vec2 pos, glm::vec2 vol);
	~Wall();
	void InitThis();

	void SetPos( glm::vec2 pos );
	glm::vec2 GetPos();

	void SetVol( glm::vec2 vol );
	glm::vec2 GetVol();

	float GetLeft();
	float GetRight();
	float GetDown();
	float GetUp();

private:
	glm::vec2 m_pos;
	glm::vec2 m_vol;
};

