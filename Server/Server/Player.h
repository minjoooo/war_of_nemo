#pragma once
#include "Object.h"

class Player :public Object
{
public:
	Player();
	~Player();

	void SetLife( int life );
	int GetLife();
	void MinusLife();

	bool CanShootBullet();
	void ResetBulletCoolTime();

	void SetKeyW( bool input );
	void SetKeyS( bool input );
	void SetKeyA( bool input );
	void SetKeyD( bool input );

	bool GetKeyW();
	bool GetKeyS();
	bool GetKeyA();
	bool GetKeyD();

private:
	int m_life;

	float m_bulletCooltime = 0.f;
	float m_reaminingCooltime = 0.2f;

	bool m_keyW = false;
	bool m_keyS = false;
	bool m_keyA = false;
	bool m_keyD = false;
};

