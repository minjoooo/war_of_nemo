#include "pch.h"
#include "Object.h"

Object::Object()
{
	m_color = glm::vec4( -1.f, -1.f, -1.f, -1.f );
	m_pos = glm::vec2( -1.f, -1.f );
	m_prepos = glm::vec2( -1.f, -1.f );
	m_vol = glm::vec2( -1.f, -1.f );
	m_vel = glm::vec2( 0.f, 0.f );
	m_acc = glm::vec2( -1.f, -1.f );
	m_mass = -1.f;
	m_fricCoef = 0.f;
}

Object::~Object()
{
}

void Object::InitPlayer()
{
	m_vol = glm::vec2( 30.f, 30.f );//hero
	m_mass = 10.f;
	m_fricCoef = 0.7f;
}

void Object::InitBullet()
{
	m_vol = glm::vec2( 0.05f, 0.05f );//hero
	m_mass = 1.f;
	m_fricCoef = 0.0f;
}

void Object::Update( float elapsedTime )
{
	float Fn = GRAVITY * m_mass;
	float Friction = m_fricCoef * Fn;

	//normalized velocity vector for getting direction of object
	float velSize = sqrtf( m_vel.x * m_vel.x + m_vel.y * m_vel.y );
	if ( velSize > 0.00001f )
	{
		glm::vec2 dir = m_vel / velSize;

		//calculate friction force
		glm::vec2 friction = -dir * Friction;

		//calculate friction acc
		glm::vec2 acc = friction / m_mass;

		//Update velocity by friction force
		glm::vec2 newVel = m_vel + acc * elapsedTime;

		if ( newVel.x * m_vel.x < 0.f )
		{
			m_vel.x = 0.f;
		}
		else
		{
			m_vel.x = newVel.x;
		}

		if ( newVel.y * m_vel.y < 0.f )
		{
			m_vel.y = 0.f;
		}
		else
		{
			m_vel.y = newVel.y;
		}
	}

	glm::vec2 pos = m_pos + m_vel * elapsedTime;

	//set prepos
	m_prepos = m_pos;

	//Update position
	m_pos += m_vel * elapsedTime;
}

void Object::AddForce( glm::vec2 force, float elapsedTime )
{
	glm::vec2 acc = force / m_mass;

	float vel_size = sqrtf( pow( m_vel.x, 2 ) + pow( m_vel.y, 2 ) );
	if( vel_size < 25.f )
		m_vel = m_vel + acc * elapsedTime;
	else
		m_vel = m_vel;
}

void Object::InverseVX()
{
	m_vel.x = -m_vel.x;
}
void Object::InverseVY()
{
	m_vel.y = -m_vel.y;
}

void Object::SetPos( glm::vec2& pos )
{
	m_pos = pos;
}
glm::vec2 Object::GetPos()
{
	return m_pos;
}

glm::vec2 Object::GetPrePos()
{
	return m_prepos;
}

void Object::SetPosX( const float& pos )
{
	m_pos.x = pos;
}
void Object::SetPosY( const float& pos )
{
	m_pos.y = pos;
}

void Object::SetVol( glm::vec2 vol )
{
	m_vol = vol;
}
glm::vec2 Object::GetVol()
{
	return m_vol;
}

void Object::SetVel( glm::vec2 vel )
{
	m_vel = vel;
}
glm::vec2 Object::GetVel()
{
	return m_vel;
}

void Object::SetAcc( glm::vec2 acc )
{
	m_acc = acc;
}
glm::vec2 Object::GetAcc()
{
	return m_acc;
}

void Object::SetColor( glm::vec4 color )
{
	m_color = color;
}
glm::vec4 Object::GetColor()
{
	return m_color;
}

void Object::SetMass( float mass )
{
	m_mass = mass;
}
float Object::GetMass()
{
	return m_mass;
}

void Object::SetFricCoef( float fricCoef )
{
	m_fricCoef = fricCoef;
}
float Object::GetFricCoef()
{
	return m_fricCoef;
}
