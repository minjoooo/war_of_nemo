#pragma once
#include "Player.h"
#include "Bullet.h"
#include "Wall.h"

struct Message {
	char id;
	char type;
	char dir;
	bool isPushed;
};

struct PlayerReadyInfo {
	char id;
	char ready;
};

//=== 접속한 클라이언트를 관리하는 구조체===
struct ConnectClient {
	bool is_connected;
};
ConnectClient g_connectedCls[3] { false };
// =======================================

std::vector <Player> phyPlayers;
std::vector <Bullet> phyBullets;

Wall walls[9];
wallinfo wallpos[9] = { {100, 100}, {250, 100}, {400, 100},
				  {100, 250}, {250, 250}, {400, 250},
				  {100, 400}, {250, 400}, {400, 400} };

// 플레이어들의 초기위치
Pro_Player g_initialPos[3] = { {0, 30, 60}, {1, 460, 60}, {2, 200, 450} };

PlayerReadyInfo g_playerReadyInfo[3] { {-1, 0}, {-1, 0}, {-1, 0} };

std::queue <Message> g_MsgQueue;

std::map <char, SOCKET> g_clients;

atomic_int numOfCls;				// 접속한 클라이언트 수

bool g_isPlaying = false;			// 게임 상태(접속가능, 불가능)

// mutex 객체
mutex g_MsgQueueLock;
mutex g_connectedClsLock;
mutex g_socketLock;
mutex g_playerReadyInfoLock;
mutex g_initialPosLock;