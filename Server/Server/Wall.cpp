#include "pch.h"
#include "Wall.h"

Wall::Wall(){ InitThis(); }
Wall::Wall( glm::vec2 pos, glm::vec2 vol ) : m_pos{ pos }, m_vol{ vol }{}
Wall::~Wall()
{

}
void Wall::InitThis()
{
	m_pos = glm::vec2( -1.f, -1.f );
	m_vol = glm::vec2( -1.f, -1.f );
}

void Wall::SetPos( glm::vec2 pos )
{
	m_pos = pos;
}
glm::vec2 Wall::GetPos()
{
	return m_pos;
}

void Wall::SetVol( glm::vec2 vol )
{
	m_vol = vol;
}
glm::vec2 Wall::GetVol()
{
	return m_vol;
}

float Wall::GetLeft()
{
	return m_pos.x - ( m_vol.x / 2 );
}
float Wall::GetRight()
{
	return m_pos.x + ( m_vol.x / 2 );
}
float Wall::GetUp()
{
	return m_pos.y - ( m_vol.y / 2 );
}
float Wall::GetDown()
{
	return m_pos.y + ( m_vol.y / 2 );
}